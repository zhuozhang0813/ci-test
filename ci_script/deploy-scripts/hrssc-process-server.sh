#!/bin/bash
#根据环境区分部署的IP数组值。环境区分是传入的env参数及选取的分支决定。
#替换argv值即可。
if [[ $env == 'test' && $CI_BUILD_REF_NAME == 'develop' ]]; then
   argv=(10.129.36.197)
elif [[ $env == 'dev' ]] && [[ $CI_BUILD_REF_NAME == 'feature' ]]; then
   argv=(10.129.36.145)
elif [[ $env == 'prod' ]] && [[ $CI_BUILD_REF_NAME == 'master' || $CI_BUILD_REF_NAME =~ [v|V][0-9]+\.[0-9]+\.[0-9]+$ ]]; then
   argv=(生产IP数组)
else
   echo 'Please enter environment parameters test or prod !'
   exit 1
fi
echo ${argv[@]}
echo "当前环境为-->UAT...."
#遍历IP并部署：部署先将gitlab打的包放到机器的/tmp目录下，然后登陆目标机器将包放到部署目录，并删除/tmp下内容。
# 在编译此部分时，注意修改服务名称(如：hrssc-process)，先执行停止原有服务脚本，再执行启动脚本
for i in "${argv[@]}";do
    scp -o StrictHostKeyChecking=no $CI_PROJECT_DIR/publish/* cifiadmin@$i:/tmp/ && \
    ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no cifiadmin@$i "
    source /etc/profile ;
    ps aux |grep 'java'|grep 'hrssc-process'|grep -v 'grep' ;
    /app/stop-service.sh 'hrssc-process' || exit 1 ;
    \cp /tmp/hrssc-process.jar /app/ ;
    /app/start-service.sh || exit 1 ;
    rm -f /tmp/hrssc-process.jar ;
    ps aux |grep 'java'|grep 'hrssc-process'| grep -v 'grep' ;
    exit 0 ;" || exit 1
    echo "operate $i "
done
echo 'deploy successful!'
