#!/bin/bash
echo $CI_PROJECT_PATH_SLUG
#编译后将包拷贝至新建的publish目录下，gitlab上可以在对应节点下载包。
    echo "build maven【UAT】....."
    mvn clean package -Dmaven.test.skip=true || exit 1 && \
    # 获取构建的jar包路径
    jarpath=$(find . -name "hrssc-process*.jar") && \
    echo "build jarpath...."
    # 在runner创建目录且将jar包移动到目录中
    mkdir -p publish && \
    mv $jarpath publish/ && \
    echo 'build successful!'
